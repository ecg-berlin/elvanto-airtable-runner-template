# Elvanto Airtable Runner Template

This is a template for an easy setup of [https://gitlab.com/ecg-berlin/elvanto-airtable](https://gitlab.com/ecg-berlin/elvanto-airtable/issues)

## How to use it

Fork this project. Make it private in Settings -> General.

Then modify services.yml

## Options for services.yml

